.PHONY: install

install: 
	cp -vfr .bashrc.d bin .gitconfig ${HOME}
	find ${HOME}/.bashrc.d -type f -exec chmod a+x {} \;
	find ${HOME}/bin -type f -exec chmod a+x {} \;
	cat .bashrc >> ${HOME}/.bashrc
