if [ -d ~/.bashrc.d ]; then
    for script in ~/.bashrc.d/*; do
        test -f $script && source $script;
        test -f ~/.bash_$(basename $script) && source ~/.bash_$(basename $script);
    done;
fi
